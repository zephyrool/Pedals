EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PT2399 U?
U 1 1 5AA1CD31
P 3200 4700
F 0 "U?" H 3200 5400 50  0000 C CNN
F 1 "PT2399" H 3200 4000 50  0000 C CNN
F 2 "" H 3200 4300 50  0001 C CNN
F 3 "" H 3200 4300 50  0001 C CNN
	1    3200 4700
	1    0    0    -1  
$EndComp
$Comp
L TL072 U?
U 1 1 5AA1CD61
P 2800 1350
F 0 "U?" H 2800 1550 50  0000 L CNN
F 1 "TL072" H 2800 1150 50  0000 L CNN
F 2 "" H 2800 1350 50  0001 C CNN
F 3 "" H 2800 1350 50  0001 C CNN
	1    2800 1350
	1    0    0    -1  
$EndComp
$Comp
L TL072 U?
U 2 1 5AA1CD91
P 4350 1000
F 0 "U?" H 4350 1200 50  0000 L CNN
F 1 "TL072" H 4350 800 50  0000 L CNN
F 2 "" H 4350 1000 50  0001 C CNN
F 3 "" H 4350 1000 50  0001 C CNN
	2    4350 1000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
